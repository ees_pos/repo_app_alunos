package br.com.appaluno.service;

import java.util.List;

import br.com.appaluno.vo.Aluno;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by ricardosouza on 3/30/17.
 */

public interface AlunoService {

    public static final String BASE_URL = "https://serverappalunos.herokuapp.com/webresources/";

    @GET("alunos")
    Call<List<Aluno>> getAlunos();
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })

    @GET("alunos/{id}")
    Call<Aluno> pesquisarAluno(@Path("id") int id);;
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("alunos")
    Call<ResponseBody> adicionarAluno(@Body Aluno aluno);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("alunos")
    Call<ResponseBody> atualizarAluno(@Body Aluno aluno);

    @DELETE("alunos/{id}")
    Call<Aluno>removerAluno(@Path("id") int id);



}
