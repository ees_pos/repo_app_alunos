package br.com.appaluno.utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.appaluno.activities.MainActivity;
import br.com.appaluno.R;

/**
 * Created by ricardosouza on 3/30/17.
 */

public class NoInternetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_internet_activity);
    }

    public void tentarNovamente(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
