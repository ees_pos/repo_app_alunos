package br.com.appaluno.activities;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.appaluno.R;
import br.com.appaluno.fragments.AlunoFragment;
import br.com.appaluno.utils.NoInternetActivity;
import br.com.appaluno.vo.Aluno;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se estiver offline, carrega a tela com a mensagem de offline
        if (!isOnline()) {
            startActivity(new Intent(this, NoInternetActivity.class));
            finish();
            return;
        }

        //instancia uma toolbar (barra de busca) e seta ela para a tela
        setTitle("");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityPersisteAluno.class);
                AlunoFragment alunoFragment = (AlunoFragment) getSupportFragmentManager().findFragmentById(R.id.content);
                if (!alunoFragment.getMatriculaSelecionada().isEmpty()) {
                    intent.putExtra("matricula", alunoFragment.getMatriculaSelecionada());
                }
                startActivity(intent);
                System.gc();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment fragment = null;
        Class fragmentClass = AlunoFragment.class;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment != null)
            fragment.setArguments(getIntent().getExtras());

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();

    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getMenuInflater().inflate(R.menu.search, menu);


        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();

        //Seta a view para permitir somente numeros (matricula).
        searchView.setInputType(2);
        searchView.setQueryHint("Matricula..");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.isEmpty()) { //se a pesquisa não for vazia, busca o aluno no servidor
                    AlunoFragment alunoFragment = (AlunoFragment) getSupportFragmentManager().findFragmentById(R.id.content);
                    alunoFragment.pesquisarAluno(Integer.valueOf(query));
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                //Toast.makeText(MainActivity.this, "False", Toast.LENGTH_SHORT).show();

                //se o tamanho da query for zero, exibe todos os itens
                if (searchView.getQuery().length() == 0) {
                    AlunoFragment alunoFragment = (AlunoFragment) getSupportFragmentManager().findFragmentById(R.id.content);
                    alunoFragment.getAlunos();
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sobre) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void remove(View view) {
        AlunoFragment alunoFragment = (AlunoFragment) getSupportFragmentManager().findFragmentById(R.id.content);
        if (alunoFragment.getMatriculaSelecionada().isEmpty())
            Toast.makeText(MainActivity.this, "Nada Selecionado!", Toast.LENGTH_SHORT).show();
        else {
            for (int i = 0; i < alunoFragment.getMatriculaSelecionada().size(); i++) {
                alunoFragment.removerAluno(alunoFragment.getMatriculaSelecionada().get(i));
            }
        }
    }
}
