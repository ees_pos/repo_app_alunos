package br.com.appaluno.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.appaluno.R;
import br.com.appaluno.service.AlunoService;
import br.com.appaluno.vo.Aluno;
import br.com.appaluno.vo.Endereco;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ricardosouza on 4/2/17.
 */

public class ActivityPersisteAluno extends AppCompatActivity {

    EditText txtNome, txtCpf, txtIdade, txtLogradouro, txtNumero,
            txtComplemento, txtBairro, txtCep, txtCidade, txtEstado;
    Button btnProcessar;

    private int matricula;

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persiste_aluno);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPersisteALuno);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        txtNome = (EditText) findViewById(R.id.edtNome);
        txtCpf = (EditText) findViewById(R.id.edtCpf);
        txtIdade = (EditText) findViewById(R.id.edtIdade);
        txtLogradouro = (EditText) findViewById(R.id.edtLogradouro);
        txtNumero = (EditText) findViewById(R.id.edtNumero);
        txtComplemento = (EditText) findViewById(R.id.edtComplemento);
        txtBairro = (EditText) findViewById(R.id.edtBairro);
        txtCep = (EditText) findViewById(R.id.edtCep);
        txtCidade = (EditText) findViewById(R.id.edtCidade);
        txtEstado = (EditText) findViewById(R.id.edtEstado);

        btnProcessar = (Button) findViewById(R.id.btnProcessar);

        String titulo = "";
        Intent it = getIntent();
        if (it != null) {
            Bundle params = it.getExtras();
            if (params != null) {
                titulo = "Editando aluno...";
                btnProcessar.setText("E D I T A R");
                setMatricula(it.getIntExtra("matricula", 0));
                txtNome.setText(it.getStringExtra("nome"));
                txtCpf.setText(it.getStringExtra("cpf"));
                txtIdade.setText(String.valueOf(it.getIntExtra("idade", 0)));
                txtLogradouro.setText(it.getStringExtra("logradouro"));
                txtNumero.setText(String.valueOf(it.getIntExtra("numero", 0)));
                txtComplemento.setText(it.getStringExtra("complemento"));
                txtBairro.setText(it.getStringExtra("bairro"));
                txtCep.setText(it.getStringExtra("cep"));
                txtCidade.setText(it.getStringExtra("cidade"));
                txtEstado.setText(it.getStringExtra("estado"));
            } else {
                titulo = "Novo aluno...";
                btnProcessar.setText("C A D A S T R A R");
            }
        }

        setTitle(titulo);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void processar(View view) {
        if (getMatricula() != 0)
            editar();
        else
            inserir();
    }

    protected void editar() {

        Aluno aluno = retornaAluno();
        aluno.setMatricula(getMatricula());
        if (aluno != null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AlunoService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            AlunoService service = retrofit.create(AlunoService.class);
            Call<ResponseBody> adicionarAlunoService = service.atualizarAluno(aluno);
            adicionarAlunoService.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccess()) {
                        Toast.makeText(getApplicationContext(), "Cadastro atualizado com sucesso!", Toast.LENGTH_SHORT).show();
                        ;
                        Intent it = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(it);
                    } else {
                        Toast.makeText(getApplicationContext(), "Problemas ao atualizar aluno!", Toast.LENGTH_SHORT).show();
                        ;
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Erro ao atualizar aluno!", Toast.LENGTH_SHORT).show();
                    ;
                }
            });
        }
    }

    protected void inserir() {

        Aluno aluno = retornaAluno();

        if (aluno != null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AlunoService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            AlunoService service = retrofit.create(AlunoService.class);
            Call<ResponseBody> adicionarAlunoService = service.adicionarAluno(aluno);
            adicionarAlunoService.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccess()) {
                        Toast.makeText(getApplicationContext(), "Cadastro realizado com sucesso!", Toast.LENGTH_SHORT).show();
                        ;
                        Intent it = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(it);
                    } else {
                        Toast.makeText(getApplicationContext(), "Problemas ao adicionar novo aluno!", Toast.LENGTH_SHORT).show();
                        ;
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Erro ao adicionar novo aluno!", Toast.LENGTH_SHORT).show();
                    ;
                }
            });
        }
    }

    protected Aluno retornaAluno() {
        if (txtNome.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Preencha o nome!", Toast.LENGTH_SHORT).show();
            ;
            return null;
        }

        int idade = 0;
        if (!txtIdade.getText().toString().trim().equals(""))
            idade = Integer.parseInt(txtIdade.getText().toString().trim());

        int numero = 0;
        if (!txtNumero.getText().toString().trim().equals(""))
            numero = Integer.parseInt(txtNumero.getText().toString());

        String nome = txtNome.getText().toString();
        String cpf = txtCpf.getText().toString();
        String logradouro = txtLogradouro.getText().toString();
        String complemento = txtComplemento.getText().toString();
        String bairro = txtBairro.getText().toString();
        String cep = txtCep.getText().toString();
        String cidade = txtCidade.getText().toString();
        String estado = txtEstado.getText().toString();

        Endereco endereco = new Endereco(logradouro, numero, complemento, bairro, cep, cidade, estado);
        Aluno aluno = new Aluno(0, cpf, nome, idade, endereco);
        return aluno;
    }
}
