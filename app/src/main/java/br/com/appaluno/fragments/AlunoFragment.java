package br.com.appaluno.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.appaluno.R;
import br.com.appaluno.adapters.AlunoAdapter;
import br.com.appaluno.service.AlunoService;
import br.com.appaluno.vo.Aluno;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ricardosouza on 3/18/17.
 */

public class AlunoFragment extends Fragment {

    private ArrayList<Aluno> alunos;
    private ProgressBar progressBar;

    private RecyclerView mRecycleView;
    public static Button btnDelete;

    public AlunoFragment() {
        // Required empty public constructor
    }

    public static Button getBtnDelete(){
        return btnDelete;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_alunos, container, false);

        mRecycleView = (RecyclerView) view.findViewById(R.id.myRecycleView);
        mRecycleView.setHasFixedSize(true);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));

        btnDelete = (Button) view.findViewById(R.id.btnDelete);

        progressBar = (ProgressBar) view.findViewById(R.id.alunosProgressBar);
        alunos = new ArrayList<>();
        progressBar.setVisibility(View.VISIBLE);

        getAlunos();

        return view;
    }

    public void getAlunos() {

        Retrofit retrofit = new Retrofit.Builder()
                //passa url
                .baseUrl(AlunoService.BASE_URL)
                //passa o conversor Gson
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //faz a intância da interface
        AlunoService service = retrofit.create(AlunoService.class);
        Call<List<Aluno>> getAlunos = service.getAlunos();
        //callbacks
        getAlunos.enqueue(new Callback<List<Aluno>>() {
            @Override
            public void onResponse(Call<List<Aluno>> call, Response<List<Aluno>> response) {

                if(response.isSuccess()) {
                    List<Aluno> listaAlunos = response.body();
                    //limpa a lista local
                    alunos.clear();
                    alunos.addAll(listaAlunos);
                    mRecycleView.setAdapter(new AlunoAdapter(getContext(), alunos));

                }else{
                    Toast.makeText(getContext(), "Erro ao receber listagem de alunos.", Toast.LENGTH_SHORT).show();;
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<List<Aluno>> call, Throwable t) {
                Toast.makeText(getContext().getApplicationContext(), "Erro ao receber listagem de alunos.", Toast.LENGTH_SHORT).show();;
            }
        });
    }

    public void removerAluno(int id) {
        Retrofit retrofit = new Retrofit.Builder()
                //passa a url base
                .baseUrl(AlunoService.BASE_URL)
                //passa o conversor Gson
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //faz a intância da interface e instância um objeto aluno que sera o retorno
        AlunoService service = retrofit.create(AlunoService.class);
        Call<Aluno> removerAluno = service.removerAluno(id);
        Aluno alunoApagado = new Aluno();

        //Executa a função enviando a id(matricula) e recebe o retorno (objeto aluno)
        removerAluno.enqueue(new Callback<Aluno>() {
        @Override
            public void onResponse(Call<Aluno> call, Response<Aluno> response) {
                if(response.isSuccess()) {
                    AlunoAdapter alunoAdapter = (AlunoAdapter) mRecycleView.getAdapter();
                    alunoAdapter.removerAlunoLista();
                    Aluno alunoApagado = response.body();
                    if (alunoApagado!=null){
                        Toast.makeText(getContext(), "Aluno removido com sucesso!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getContext(), "Erro ao apagar aluno.", Toast.LENGTH_SHORT).show();;
                }
            }
            @Override
            public void onFailure(Call<Aluno> call, Throwable t) {
                Toast.makeText(getContext(), "Erro ao receber retorno", Toast.LENGTH_SHORT).show();;
            }

        });
    }

    public ArrayList<Integer> getMatriculaSelecionada(){

        return(( (AlunoAdapter) mRecycleView.getAdapter()).getMatriculaSel() );
    }

    public void pesquisarAluno(int id) {
        Retrofit retrofit = new Retrofit.Builder()
        //passa a url base
        .baseUrl(AlunoService.BASE_URL)
        //passa o conversor Gson
        .addConverterFactory(GsonConverterFactory.create())
        .build();

        //faz a intância da interface e instância um objeto aluno que sera o retorno
        AlunoService service = retrofit.create(AlunoService.class);
        Call<Aluno> pesquisarAluno = service.pesquisarAluno(id);

        //Executa a função enviando a id(matricula) e recebe o retorno (objeto aluno)
        pesquisarAluno.enqueue(new Callback<Aluno>() {
            @Override
            public void onResponse(Call<Aluno> call, Response<Aluno> response) {
                if(response.isSuccess()) {
                    AlunoAdapter alunoAdapter = (AlunoAdapter) mRecycleView.getAdapter();
                    Aluno aluno = response.body();
                    if(aluno!=null){
                        alunoAdapter.atualizarPesquisa(aluno);
                    }else{
                        Toast.makeText(getContext(), "Nenhum aluno encontrado!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Aluno> call, Throwable t) {
                Toast.makeText(getContext(), "Erro ao receber retorno", Toast.LENGTH_SHORT).show();
            }

        });
    }

}
