package br.com.appaluno.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.appaluno.R;
import br.com.appaluno.activities.ActivityPersisteAluno;
import br.com.appaluno.fragments.AlunoFragment;
import br.com.appaluno.vo.Aluno;

/**
 *
 */

public class AlunoAdapter extends RecyclerView.Adapter<AlunoAdapter.ViewHolder>  {

    private static ArrayList<Aluno> listaAlunos;
    private Context context;
    private static ArrayList<Integer> matriculaSel = new ArrayList<>();
    private static int position;

    public static ArrayList<Integer> getMatriculaSel() {
        return matriculaSel;
    }

    public AlunoAdapter(Context context, ArrayList<Aluno> listAlunos) {
        this.context = context;
        this.listaAlunos = listAlunos;
        this.position = -1;
    }

    static class ViewHolder extends  RecyclerView.ViewHolder {

        View viewItem;
        TextView nome, email;
        CardView card;
        CheckBox chkDelete;

        ViewHolder(View viewItem) {
            super(viewItem);
            this.viewItem = viewItem;
            nome = (TextView) viewItem.findViewById(R.id.txtNome);
            email = (TextView) viewItem.findViewById(R.id.txtEmail);
            chkDelete = (CheckBox) viewItem.findViewById(R.id.chkDelete);

            card = (CardView) viewItem.findViewById(R.id.alunoCard);
            card.setCardBackgroundColor(Color.WHITE);

            chkDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    Aluno aluno = listaAlunos.get(position);

                    if(chkDelete.isChecked()){
                        if (position != RecyclerView.NO_POSITION) {
                            matriculaSel.add(aluno.getMatricula());
                            AlunoFragment.getBtnDelete().setVisibility(View.VISIBLE);
                        }
                    }else{
                        if(getMatriculaSel().contains(aluno.getMatricula()))
                            matriculaSel.remove( (Integer) aluno.getMatricula() );
                    }

                    if(getMatriculaSel().isEmpty())
                        AlunoFragment.getBtnDelete().setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext().getApplicationContext()).inflate(R.layout.content_alunos, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nome.setText("Matrícula: " + listaAlunos.get(position).getMatricula() );
        holder.email.setText("Nome: " + listaAlunos.get(position).getNome());

        final int p = position;
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityPersisteAluno.class);
                intent.putExtra("matricula", listaAlunos.get(p).getMatricula());
                intent.putExtra("nome", listaAlunos.get(p).getNome());
                intent.putExtra("cpf", listaAlunos.get(p).getCpf());
                intent.putExtra("idade", listaAlunos.get(p).getIdade());

                intent.putExtra("logradouro", listaAlunos.get(p).getEndereco().getLogradouro());
                intent.putExtra("numero", listaAlunos.get(p).getEndereco().getNumero() );
                intent.putExtra("complemento", listaAlunos.get(p).getEndereco().getComplemento());
                intent.putExtra("bairro", listaAlunos.get(p).getEndereco().getBairro());
                intent.putExtra("cep", listaAlunos.get(p).getEndereco().getCep());
                intent.putExtra("cidade", listaAlunos.get(p).getEndereco().getCidade());
                intent.putExtra("estado", listaAlunos.get(p).getEndereco().getEstado());

                context.startActivity(intent);
                System.gc();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaAlunos.size();
    }

    public void removerAlunoLista() {
        listaAlunos.remove(position);
        notifyItemRemoved(position);
    }

    public void atualizarPesquisa(Aluno aluno) {
        listaAlunos.clear();
        listaAlunos.add(aluno);
        notifyDataSetChanged();
    }

}
